﻿/*
This file is part of PInvoke.

PInvoke is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PInvoke is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PInvoke.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;

namespace ContentTypeTextNet.Library.PInvoke.Windows
{
    partial class NativeMethods
    {
        public static Guid IID_IImageList = new Guid("{46EB5926-582E-4017-9FDF-E8998DAA0950}");
        public static Guid IID_IShellItem = new Guid("{43826d1e-e718-42ee-bc55-a1e261c37bfe}");
        public static Guid IID_IShellLink = new Guid("{000214F9-0000-0000-C000-000000000046}");
    }
}

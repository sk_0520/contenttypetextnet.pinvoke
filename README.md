# ContentTypeTextNet.PInvoke

ContentTypeTextNet.PInvoke は P/Invoke として現在のところ  Windows を対象とした API を定義しています。
アセンブリ名は **ContentTypeTextNet.PInvoke** です。

 * [Issues](https://bitbucket.org/sk_0520/contenttypetextnet.sharedlibrary/issues?status=new&status=open)を参照。
 * [![myget](https://www.myget.org/BuildSource/Badge/content-type-text-net?identifier=1ada84f0-42db-4b16-9737-f535af5d84dd)](https://www.myget.org/gallery/content-type-text-net)

## 参照

 * [Pe](https://bitbucket.org/sk_0520/pe)

